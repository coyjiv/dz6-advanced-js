"use strict";
const loader = document.querySelector(".loader");
const button = document.getElementById("send");
button.addEventListener("click", getAndShowIp);
let change = 0;
async function getAndShowIp() {
  if (change === 1) {
    alert("Ну мы ведь и так уже всё узнали)");
    return;
  }
  loader.classList.remove("hide");
  let response = await axios.get("https://api.ipify.org/?format=json");
  if (response.status >= 200 && response.status <= 300) {
    loader.classList.add("hide");
    change = 1;
  }
  let info = await response.data.ip;
  let locationResponse = await axios.get(
    "http://ip-api.com/json/" + info + "?fields=1589277"
  );
  const { continent, country, region, city, regionName, district } =
    locationResponse.data;
  const p = document.createElement("p");
  p.textContent = `${continent}, ${country}, ${region}, ${city}, ${regionName} ${district}`;
  p.classList.add("stylish");
  loader.after(p);
}
